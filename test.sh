#!/usr/bin/bash
set -e

function bench
{
	#run f16c codelets
	cat ref.raw | ./expand | ./compress > res.raw

	#check accuracy
	diff ref.raw res.raw

	if ((0 == $?))
	then
		echo "test passed. bye"
	fi
}

#random
python << EOF > ref.raw
import numpy as np
x = np.random.uniform(-1e4, 1e4, 654321).astype(np.float16)
x.tofile("/dev/stdout")
EOF

bench

#small, not multiple of 8
python << EOF > ref.raw
import numpy as np
x = np.arange(17, dtype=np.float16)
x.tofile("/dev/stdout")
EOF

bench

#exhaustive
python << EOF > ref.raw
import numpy as np
x = np.arange(65536, dtype=np.uint16).astype(np.float16)
x.tofile("/dev/stdout")
EOF

bench
