#include <stdint.h>

#include <immintrin.h>

#include "f16c.h"

void f16c_compress (
    const float * const in,
    const ptrdiff_t n,
    int16_t * const out)
{
    const ptrdiff_t nnice = n & ~7;

    for(ptrdiff_t i = 0; i < nnice; i += 8)
    {
		const __m256 x = _mm256_loadu_ps(in + i);
		const __m128i y = _mm256_cvtps_ph(x, 0);

		_mm_storeu_si128((__m128i *)(out + i), y);
    }

    const ptrdiff_t nrem = n - nnice;

    if (nrem)
    {
		float __attribute__((aligned(32))) src[8];

		for(ptrdiff_t i = 0; i < nrem; ++i)
			src[i] = in[nnice + i];

		const __m256 x = _mm256_load_ps(src);
		const __m128i y = _mm256_cvtps_ph(x, 0);

		int16_t __attribute__((aligned(32))) dst[8];
		_mm_store_si128((__m128i *)dst, y);

		for(ptrdiff_t i = 0; i < nrem; ++i)
			out[nnice + i] = dst[i];
    }
}

void f16c_expand (
    const int16_t * const in,
    const ptrdiff_t n,
    float * const out)
{
    const ptrdiff_t nnice = n & ~7;
	const ptrdiff_t nrem = n - nnice;

    if (nrem)
    {
		int16_t __attribute__((aligned(32))) src[8];

		for(ptrdiff_t i = 0; i < nrem; ++i)
			src[i] = in[nnice + i];

		const __m128i x = _mm_load_si128((__m128i *)src);
		const __m256 y = _mm256_cvtph_ps(x);

		float __attribute__((aligned(32))) dst[8];
		_mm256_store_ps(dst, y);

		for(ptrdiff_t i = 0; i < nrem; ++i)
			out[nnice + i] = dst[i];
    }

    for(ptrdiff_t i = nnice - 8; i >= 0; i -= 8)
    {
		const __m128i x = _mm_loadu_si128((__m128i *)(in + i));
		const __m256 y = _mm256_cvtph_ps(x);

		_mm256_storeu_ps(out + i, y);
    }
}
