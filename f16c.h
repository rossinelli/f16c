#include <stddef.h>

/* note that both functions 
   work correctly in-place */

void f16c_compress (
    const float * const in,
    const ptrdiff_t n,
    int16_t * const out);

void f16c_expand (
    const int16_t * const in,
    const ptrdiff_t n,
    float * const out);

