CFLAGS += -std=c99
CFLAGS += -mavx2
CFLAGS += -mf16c
CFLAGS += -march=core-avx2 
CFLAGS += -Ofast -DNDEBUG

GOALS = compress expand f16c.o

all : $(GOALS)

% : %.o f16c.o
	$(CC) $(CFLAGS) -o $@ $^

%.o : %.c
	$(CC) $(CFLAGS) -c $<

clean : 
	rm -f $(GOALS) *.o

.PHONY: clean
.SUFFIXES: 
