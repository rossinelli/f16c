#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <immintrin.h>

#include "f16c.h"

int main (
    const int argc,
    const char * const argv[])
{
    if (argc != 1)
    {
		fprintf(stderr,
				"usage: %s [STDIN] [STDOUT]\n",
				argv[0]);

		return EXIT_FAILURE;
    }

    enum { N = 16 << 10 };

    while (1)
    {
		int16_t in[N];

		const int n = fread(in, sizeof(int16_t), N, stdin);

		float out[N];

		f16c_expand(in, n, out);

		fwrite(out, sizeof(float), n, stdout);

		if (n != N)
			break;
    }

    return EXIT_SUCCESS;
}
